### 0.0.5

[x] Separate tests
[x] Fix non-existent file issue
[x] Add Hatchling
[_] Update Readme to use Nexus
[x] Update to PyQT6
[_] Fix Gitlab CI tests
[_] Release 0.0.5 to Nexus
[_] Create Gentoo Overlay and Submit

### Next

[_] Backport rich + logging from mediahug