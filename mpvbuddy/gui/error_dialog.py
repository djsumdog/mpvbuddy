# mpvbuddy
# License: AGPLv3
# https://battlepenguin.com
from PyQt6.QtWidgets import QMessageBox


def show_error(msg: str) -> None:
    msg_box = QMessageBox()
    msg_box.setIcon(QMessageBox.Icon.Critical)
    msg_box.setText(msg)
    msg_box.setWindowTitle("Error")
    msg_box.setStandardButtons(QMessageBox.StandardButton.Ok)
    msg_box.exec()
