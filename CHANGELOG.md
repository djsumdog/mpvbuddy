# Changelog

### [0.1.0] - (2025-01-22)

* Migrated the project to uv from setuptools
* Migrated from PyQt5 -> PyQt6
* Replaced wildcard with explicit imports
* Fix lockup on file read error
* Added mouse seeking

### [v0.0.1 - v0.0.4] (2019-02-22)

* Initial Alpha release