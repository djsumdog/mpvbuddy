# mpvbuddy

![PyPI](https://img.shields.io/pypi/v/mpvbuddy.svg)

Mpvbuddy is a frontend for the media player [mpv](https://mpv.io). It's main feature is the ability to keep track of where you are in every video on your playlist so you can jump around between videos you are currently watching. It supports multiple playlists as well.

It's still in early beta and is written in Python + PyQt6. Please include and messages from the console and logging window (Ctrl+U) when filing issues.

![Screenshot of mpvbuddy with playlist and video window](screenshots/mpvbuddy-example-1.png)


# Installation

Mpvbuddy is dependent on PyQt6. There are several installation methods.

Virtual environment using `pip`:
```commandline
python3 -m venv mpvbuddy-venv
source mpvbuddy-venv/bin/active
pip install mpvbuddy --extra-index-url https://nexus.sumit.im/repository/bp-python/simple
mpvbuddy
```

Creating a developer environment using the source (requires the `uv` build tool)
```commandline
git clone https://gitlab.com/djsumdog/mpvbuddy
cd mpvbuddy
uv sync
uv run python3 -m mediahug
```

# License

mpvbuddy is licensed under the GNU GPLv3 (2019,2025). Dependencies and their licenses can be found in the about menu. 
